package pl.jkozal.weather_forecast;

/**
 * Created by jacek on 31.03.2018.
 */

public class Config {
    public static final String APP_ID = "2fcc2a576c40a8b305ef6dd250d8352c";
    public static final String UNIT = "metric";
    public static final String FB_PARAMS = "id,first_name,picture";
    public static final String FB_FIELDS = "fields";
    public static final String FB_FIRST_NAME = "first_name";
    public static final String FB_PERMISSIONS = "public_profile";
    public static final int FB_PHOTO_SIZE = 200;
}
