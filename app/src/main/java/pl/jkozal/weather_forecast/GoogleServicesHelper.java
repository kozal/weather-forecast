package pl.jkozal.weather_forecast;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by jacek on 31.03.2018.
 */

public class GoogleServicesHelper {

    public static void checkIfUpToDate(Activity activity) {
        if(GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity) != ConnectionResult.SUCCESS)  {
            showOutdatedDialog(activity);
        }
    }

    public static void showOutdatedDialog(final Activity activity) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setTitle(R.string.outdated_google_services_title);
        alertDialog.setMessage(activity.getString(R.string.outdated_google_services_message));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.finish();
                    }
                });
        alertDialog.show();
    }
}
