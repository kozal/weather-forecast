package pl.jkozal.weather_forecast.model;

/**
 * Created by jacek on 31.03.2018.
 */

public class Wind {

    private double speed;
    private double deg;

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }
}
