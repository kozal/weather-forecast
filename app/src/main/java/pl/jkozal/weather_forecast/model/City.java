package pl.jkozal.weather_forecast.model;

/**
 * Created by jacek on 31.03.2018.
 */

public class City {

    private String name;
    private String country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
