package pl.jkozal.weather_forecast.model;

import java.util.List;

/**
 * Created by jacek on 31.03.2018.
 */

public class WeatherForecastResponse {

    private int cnt;
    private City city;
    private List<WeatherResponse> list;

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<WeatherResponse> getList() {
        return list;
    }

    public void setList(List<WeatherResponse> list) {
        this.list = list;
    }
}
