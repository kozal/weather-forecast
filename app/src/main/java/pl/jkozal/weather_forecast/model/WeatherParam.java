package pl.jkozal.weather_forecast.model;

/**
 * Created by jacek on 31.03.2018.
 */

public class WeatherParam {

    private String title;
    private String subtitle;

    public WeatherParam(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
