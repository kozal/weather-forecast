package pl.jkozal.weather_forecast.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jacek on 31.03.2018.
 */

public class WeatherResponse {

    private long dt;
    private String name;
    @SerializedName("main")
    private Weather weather;
    private Wind wind;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public List<WeatherParam> params() {
        List<WeatherParam> params = new ArrayList<>();
        params.add(new WeatherParam("location", name));
        params.add(new WeatherParam("temperature", String.valueOf(weather.getTemp())));
        params.add(new WeatherParam("pressure", String.valueOf(weather.getPressure())));
        params.add(new WeatherParam("humidity", String.valueOf(weather.getHumidity())));
        params.add(new WeatherParam("wind speed", String.valueOf(wind.getSpeed())));
        return params;
    }

    @Override
    public String toString() {
        return
            "temperature: " + String.valueOf(weather.getTemp()) + "\n" +
            "pressure: " + String.valueOf(weather.getPressure()) + "\n" +
            "humidity: " + String.valueOf(weather.getHumidity()) + "\n" +
            "wind speed: " + String.valueOf(wind.getSpeed());
    }
}
