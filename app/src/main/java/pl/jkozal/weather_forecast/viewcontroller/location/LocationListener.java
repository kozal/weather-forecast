package pl.jkozal.weather_forecast.viewcontroller.location;

import android.location.Location;

/**
 * Created by jacek on 31.03.2018.
 */

public interface LocationListener {
    void onLocation(Location location);
}
