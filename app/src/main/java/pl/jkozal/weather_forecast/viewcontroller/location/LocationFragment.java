package pl.jkozal.weather_forecast.viewcontroller.location;

import pl.jkozal.weather_forecast.viewcontroller.RestFragment;

/**
 * Created by jacek on 31.03.2018.
 */

public abstract class LocationFragment extends RestFragment {
    public abstract void onLocationPermissionGranted();
}
