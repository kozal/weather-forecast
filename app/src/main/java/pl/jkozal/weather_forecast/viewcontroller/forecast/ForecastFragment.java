package pl.jkozal.weather_forecast.viewcontroller.forecast;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import pl.jkozal.weather_forecast.viewcontroller.location.LocationFragment;
import pl.jkozal.weather_forecast.viewcontroller.main.MainActivity;
import pl.jkozal.weather_forecast.R;
import pl.jkozal.weather_forecast.model.WeatherForecastResponse;

/**
 * Created by jacek on 31.03.2018.
 */

public class ForecastFragment extends LocationFragment implements ForecastView {

    private MainActivity activity;
    private View view;
    private ForecastPresenter presenter;
    private ProgressBar progressBar;
    private ConstraintLayout content;
    private RecyclerView list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = (MainActivity) getActivity();
        view = inflater.inflate(R.layout.fragment_forecast, container, false);
        presenter = new ForecastPresenter(this, rest);
        progressBar = view.findViewById(R.id.progressBar);
        list = view.findViewById(R.id.list);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        list.setVisibility(View.VISIBLE);
    }

    @Override
    public void presentWeatherResults(WeatherForecastResponse response) {
        ForecastAdapter adapter = new ForecastAdapter(response);
        list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        list.setAdapter(adapter);
    }

    @Override
    public void onLocationPermissionGranted() {
        presenter.onStart();
        presenter.onResume();
    }

    @Override
    public boolean hasLocationAccessPermission() {
        return activity.hasLocationAccessPermission();
    }
}
