package pl.jkozal.weather_forecast.viewcontroller.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Arrays;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import pl.jkozal.weather_forecast.viewcontroller.FacebookController;
import pl.jkozal.weather_forecast.viewcontroller.location.LocationFragment;
import pl.jkozal.weather_forecast.R;
import pl.jkozal.weather_forecast.Config;

/**
 * Created by jacek on 31.03.2018.
 */

public class MainPresenter implements FacebookPresenter {

    private MainView view;
    private FacebookController facebookController;
    private CallbackManager facebookCallbackManager;
    private LocationFragment activeFragment;
    private Target picassoTarget;

    public MainPresenter(MainView view) {
        this.view = view;
        facebookController = new FacebookController(this);
        facebookCallbackManager = CallbackManager.Factory.create();
    }

    public void setActiveLocationFragment(LocationFragment fragment) {
        this.activeFragment = fragment;
    }

    public void onLocationPermissionGranted() {
        if (activeFragment != null) {
            activeFragment.onLocationPermissionGranted();
        }
    }

    public void onCallbackActivityResult(int requestCode, int resultCode, Intent data) {
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void processActionLogin() {
        if(Profile.getCurrentProfile() != null) {
            LoginManager.getInstance().logOut();
            view.setActionBarTitle(view.getActivity().getString(R.string.app_name));
            view.setSignInIcon(view.getActivity().getDrawable(R.drawable.ic_account_circle_white_24dp));
            Toast.makeText(view.getActivity(), R.string.bye, Toast.LENGTH_SHORT).show();
        } else {
            LoginManager.getInstance().logInWithReadPermissions(view.getActivity(), Arrays.asList(Config.FB_PERMISSIONS));
            facebookController.registerCallback();
        }
    }

    public void onStart() {
        if(Profile.getCurrentProfile() != null) {
            setActionBarTitle(Profile.getCurrentProfile().getFirstName());
            setLoginIconUri(Profile.getCurrentProfile().getProfilePictureUri(Config.FB_PHOTO_SIZE, Config.FB_PHOTO_SIZE));
        }
    }

    @Override
    public void setActionBarTitle(String title) {
        view.setActionBarTitle(title);
    }

    @Override
    public void setLoginIconUri(Uri uri) {
        picassoTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                view.setSignInIcon(new BitmapDrawable(view.getActivity().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                Crashlytics.logException(e);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {}
        };

        Picasso.get()
            .load(uri.toString())
            .transform(new CropCircleTransformation())
            .into(picassoTarget);
    }

    @Override
    public CallbackManager getFacebookCallbackManager() {
        return facebookCallbackManager;
    }

    @Override
    public void onFacebookLoginSuccess(String title, Uri photoUri) {
        setActionBarTitle(title);
        setLoginIconUri(photoUri);
        Toast.makeText(view.getActivity(), R.string.welcome_back, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFacebookGraphError(Exception e) {
        Crashlytics.logException(e);
        Toast.makeText(view.getActivity(), R.string.facebook_request_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFacebookRequestCancelled() {
        Toast.makeText(view.getActivity(), R.string.facebook_request_cancelled, Toast.LENGTH_SHORT).show();
    }
}
