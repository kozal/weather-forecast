package pl.jkozal.weather_forecast.viewcontroller.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;

import com.google.android.gms.location.LocationRequest;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

/**
 * Created by jacek on 31.03.2018.
 */

public class LocationController {

    private LocationListener presenter;
    private Disposable locationSubscription;
    private ReactiveLocationProvider locationProvider;

    public LocationController(LocationListener presenter, Context context) {
        this.presenter = presenter;
        this.locationProvider = new ReactiveLocationProvider(context);
    }

    @SuppressLint("MissingPermission")
    public void subscribeToLocationChanges() {
        locationSubscription = locationProvider.getUpdatedLocation(request()).subscribe(new Consumer<Location>() {
            @Override
            public void accept(Location location) throws Exception {
                presenter.onLocation(location);
            }
        });
    }

    @SuppressLint("MissingPermission")
    public void requestLastKnownLocation() {
        locationProvider.getLastKnownLocation().subscribe(new Consumer<Location>() {
            @Override
            public void accept(Location location) throws Exception {
                presenter.onLocation(location);
            }
        });
    }

    public void unsubscribe() {
        if(locationSubscription != null && !locationSubscription.isDisposed()) {
            locationSubscription.dispose();
        }
    }

    private LocationRequest request() {
        return LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setNumUpdates(5)
            .setInterval(10000);
    }
}
