package pl.jkozal.weather_forecast.viewcontroller;

import android.os.Bundle;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import pl.jkozal.weather_forecast.Config;
import pl.jkozal.weather_forecast.viewcontroller.main.FacebookPresenter;

/**
 * Created by jacek on 31.03.2018.
 */

public class FacebookController {

    private FacebookPresenter presenter;

    public FacebookController(FacebookPresenter presenter) {
        this.presenter = presenter;
    }

    public void registerCallback() {
        LoginManager.getInstance().registerCallback(presenter.getFacebookCallbackManager(), new FacebookCallback<LoginResult>() {

            private ProfileTracker mProfileTracker;

            private void processSuccess(LoginResult loginResult) {
                getFacebookData(loginResult);
            }

            @Override
            public void onSuccess(final LoginResult loginResult){
                if(Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                            this.stopTracking();
                            processSuccess(loginResult);
                        }
                    };
                }
                else {
                    processSuccess(loginResult);
                }
            }

            @Override
            public void onCancel() {
                presenter.onFacebookRequestCancelled();
            }

            @Override
            public void onError(FacebookException e) {
                presenter.onFacebookGraphError(e);
            }
        });
    }

    private void getFacebookData(LoginResult loginResult) {
        GraphRequest request = getGraphRequest(loginResult);
        Bundle parameters = new Bundle();
        parameters.putString(Config.FB_FIELDS, Config.FB_PARAMS);
        request.setParameters(parameters);
        request.executeAsync();
    }

    private GraphRequest getGraphRequest(LoginResult loginResult) {
        return GraphRequest.newMeRequest(
            loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    onGraphReceived(response);
                }
            });
    }

    private void onGraphReceived(GraphResponse response) {
        try {
            presenter.onFacebookLoginSuccess(
                response.getJSONObject().getString(Config.FB_FIRST_NAME),
                Profile.getCurrentProfile().getProfilePictureUri(Config.FB_PHOTO_SIZE, Config.FB_PHOTO_SIZE));
        } catch(JSONException e) {
            presenter.onFacebookGraphError(e);
        }
    }
}
