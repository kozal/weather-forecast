package pl.jkozal.weather_forecast.viewcontroller;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.jkozal.weather_forecast.R;
import pl.jkozal.weather_forecast.model.WeatherParam;

/**
 * Created by jacek on 31.03.2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ListHolder> {

    private List<WeatherParam> params;

    public WeatherAdapter(List<WeatherParam> params) {
        this.params = params;
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_row, parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListHolder holder, int position) {
        WeatherParam param = params.get(position);
        holder.title.setTypeface(null, Typeface.BOLD);
        holder.title.setText(param.getTitle());
        holder.subtitle.setText(param.getSubtitle());
    }

    @Override
    public int getItemCount() {
        return params.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView subtitle;

        public ListHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            subtitle = view.findViewById(R.id.subtitle);
        }
    }
}
