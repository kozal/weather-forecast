package pl.jkozal.weather_forecast.viewcontroller.main;

import android.app.Activity;
import android.graphics.drawable.Drawable;

/**
 * Created by jacek on 31.03.2018.
 */

public interface MainView {
    Activity getActivity();
    void setSignInIcon(Drawable drawable);
    void setActionBarTitle(String title);
}
