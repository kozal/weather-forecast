package pl.jkozal.weather_forecast.viewcontroller.forecast;

import android.location.Location;

import com.crashlytics.android.Crashlytics;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pl.jkozal.weather_forecast.viewcontroller.BasePresenter;
import pl.jkozal.weather_forecast.Config;
import pl.jkozal.weather_forecast.viewcontroller.location.LocationController;
import pl.jkozal.weather_forecast.viewcontroller.location.LocationListener;
import pl.jkozal.weather_forecast.backend.RestCallback;
import pl.jkozal.weather_forecast.backend.RestInterface;
import pl.jkozal.weather_forecast.backend.RestObserver;
import pl.jkozal.weather_forecast.model.WeatherForecastResponse;

/**
 * Created by jacek on 31.03.2018.
 */

public class ForecastPresenter implements BasePresenter, RestCallback<WeatherForecastResponse>, LocationListener {

    private ForecastView view;
    private RestInterface rest;
    private LocationController locationController;

    public ForecastPresenter(ForecastView view, RestInterface rest) {
        this.view = view;
        this.rest = rest;
        this.locationController = new LocationController(this, view.getContext());
    }

    @Override
    public void onStart() {
        if(view.hasLocationAccessPermission()) {
            locationController.requestLastKnownLocation();
            view.showLoading();
        }
    }

    @Override
    public void onResume() {
        if(view.hasLocationAccessPermission()) {
            locationController.subscribeToLocationChanges();
        }
    }

    @Override
    public void onPause() {
        locationController.unsubscribe();
    }

    @Override
    public void onSuccess(WeatherForecastResponse result) {
        view.presentWeatherResults(result);
        view.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        Crashlytics.logException(e);
        view.hideLoading();
    }

    @Override
    public void onLocation(Location location) {
        rest.weatherForecast(location.getLatitude(), location.getLongitude(), Config.APP_ID, Config.UNIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new RestObserver<>(this));
    }
}
