package pl.jkozal.weather_forecast.viewcontroller;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import javax.inject.Inject;
import javax.inject.Named;

import pl.jkozal.weather_forecast.WeatherForecastApp;
import pl.jkozal.weather_forecast.backend.RestInterface;

/**
 * Created by jacek on 31.03.2018.
 */

public class RestFragment extends Fragment {

    @Inject
    @Named("backend")
    protected RestInterface rest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeatherForecastApp app = (WeatherForecastApp) getActivity().getApplication();
        app.getRestComponent().inject(this);
    }
}
