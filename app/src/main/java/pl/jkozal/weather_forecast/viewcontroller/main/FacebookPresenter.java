package pl.jkozal.weather_forecast.viewcontroller.main;

import android.net.Uri;

import com.facebook.CallbackManager;

/**
 * Created by jacek on 01.04.2018.
 */

public interface FacebookPresenter {
    void setActionBarTitle(String title);
    void setLoginIconUri(Uri uri);
    CallbackManager getFacebookCallbackManager();
    void onFacebookLoginSuccess(String title, Uri photoUri);
    void onFacebookGraphError(Exception e);
    void onFacebookRequestCancelled();
}
