package pl.jkozal.weather_forecast.viewcontroller.forecast;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import pl.jkozal.weather_forecast.R;
import pl.jkozal.weather_forecast.model.WeatherForecastResponse;
import pl.jkozal.weather_forecast.model.WeatherResponse;

/**
 * Created by jacek on 31.03.2018.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ListHolder> {

    private WeatherForecastResponse response;

    public ForecastAdapter(WeatherForecastResponse response) {
        this.response = response;
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_row, parent, false);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListHolder holder, int position) {
        WeatherResponse item = response.getList().get(position);
        Date date = new Date(item.getDt() * 1000);
        DateFormat formatter = new SimpleDateFormat("dd/MM HH:mm", Locale.US);
        String dateFormatted = formatter.format(date);
        holder.title.setTypeface(null, Typeface.BOLD);
        holder.title.setText(dateFormatted);
        holder.subtitle.setText(item.toString());
    }

    @Override
    public int getItemCount() {
        return response.getList().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView subtitle;

        public ListHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            subtitle = view.findViewById(R.id.subtitle);
        }
    }
}
