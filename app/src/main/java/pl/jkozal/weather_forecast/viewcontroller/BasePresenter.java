package pl.jkozal.weather_forecast.viewcontroller;

/**
 * Created by jacek on 31.03.2018.
 */

public interface BasePresenter {
    void onStart();
    void onResume();
    void onPause();
}
