package pl.jkozal.weather_forecast.viewcontroller.main;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import pl.jkozal.weather_forecast.GoogleServicesHelper;
import pl.jkozal.weather_forecast.viewcontroller.location.LocationFragment;
import pl.jkozal.weather_forecast.R;
import pl.jkozal.weather_forecast.RequestCode;
import pl.jkozal.weather_forecast.viewcontroller.forecast.ForecastFragment;
import pl.jkozal.weather_forecast.viewcontroller.today.TodayFragment;

import static pl.jkozal.weather_forecast.RequestCode.LOCATION_PERMISSION;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MainView {

    private DrawerLayout drawer;
    private MainPresenter presenter;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initNavigationDrawer();
        initNavigationView();
        GoogleServicesHelper.checkIfUpToDate(this);
        attachFragment(new TodayFragment());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onCallbackActivityResult(requestCode, resultCode, data);
    }

    private void initNavigationDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavigationView() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.onLocationPermissionGranted();
            } else {
                Toast.makeText(this, R.string.permission_rejected, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                processOpenDrawer();
                return true;
            case R.id.action_login:
                presenter.processActionLogin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(android.view.MenuItem item) {
        attachFragment(
            item.getItemId() == R.id.weather_five_days ?
                new ForecastFragment() : new TodayFragment());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void processOpenDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void attachFragment(LocationFragment fragment) {
        presenter.setActiveLocationFragment(fragment);
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment).commit();
    }

    public boolean hasLocationAccessPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, RequestCode.LOCATION_PERMISSION);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void setActionBarTitle(String name) {
        setTitle(name);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public void setSignInIcon(Drawable drawable) {
        MenuItem menuItem = menu.findItem(R.id.action_login);
        menuItem.setIcon(drawable);
    }
}
