package pl.jkozal.weather_forecast.viewcontroller.forecast;

import pl.jkozal.weather_forecast.viewcontroller.location.LocationView;
import pl.jkozal.weather_forecast.model.WeatherForecastResponse;

/**
 * Created by jacek on 31.03.2018.
 */

public interface ForecastView extends LocationView {
    void presentWeatherResults(WeatherForecastResponse response);
}
