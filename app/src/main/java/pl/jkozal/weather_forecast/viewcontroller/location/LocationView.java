package pl.jkozal.weather_forecast.viewcontroller.location;

import android.content.Context;

/**
 * Created by jacek on 31.03.2018.
 */

public interface LocationView {
    Context getContext();
    void showLoading();
    void hideLoading();
    boolean hasLocationAccessPermission();
}
