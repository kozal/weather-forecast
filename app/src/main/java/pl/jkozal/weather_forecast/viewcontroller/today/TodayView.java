package pl.jkozal.weather_forecast.viewcontroller.today;

import pl.jkozal.weather_forecast.viewcontroller.location.LocationView;
import pl.jkozal.weather_forecast.model.WeatherResponse;

/**
 * Created by jacek on 31.03.2018.
 */

public interface TodayView extends LocationView {
    void presentWeatherResult(WeatherResponse response);
}
