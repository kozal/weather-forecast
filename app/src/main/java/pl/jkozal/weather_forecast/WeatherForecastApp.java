package pl.jkozal.weather_forecast;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import io.fabric.sdk.android.Fabric;
import pl.jkozal.weather_forecast.backend.DaggerRestComponent;
import pl.jkozal.weather_forecast.backend.RestComponent;

/**
 * Created by jacek on 31.03.2018.
 */

public class WeatherForecastApp extends Application {

    private RestComponent restComponent;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        restComponent = DaggerRestComponent.builder().build();
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public RestComponent getRestComponent() {
        return restComponent;
    }
}
