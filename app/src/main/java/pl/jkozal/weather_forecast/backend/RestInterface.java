package pl.jkozal.weather_forecast.backend;


import io.reactivex.Observable;
import pl.jkozal.weather_forecast.model.WeatherForecastResponse;
import pl.jkozal.weather_forecast.model.WeatherResponse;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jacek on 13.01.2017.
 */

public interface RestInterface {

    @POST("weather")
    Observable<WeatherResponse> weatherToday(
        @Query("lat") double lat,
        @Query("lon") double lon,
        @Query("APPID") String appId,
        @Query("units") String metric);

    @POST("forecast")
    Observable<WeatherForecastResponse> weatherForecast(
        @Query("lat") double lat,
        @Query("lon") double lon,
        @Query("APPID") String appId,
        @Query("units") String metric);
}
