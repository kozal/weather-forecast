package pl.jkozal.weather_forecast.backend;

/**
 * Created by jacek on 31.03.2018.
 */

public interface RestCallback<T> {
    void onSuccess(T result);
    void onError(Throwable e);
}
