package pl.jkozal.weather_forecast.backend;


import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by jacek on 31.03.2018.
 */

public class RestObserver<T> implements Observer<T> {

    private RestCallback<T> callback;

    public RestObserver(RestCallback<T> callback) {
        this.callback = callback;
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(T t) {
        callback.onSuccess(t);
    }

    @Override
    public void onError(Throwable e) {
        Crashlytics.logException(e);
        callback.onError(e);
    }
}
