package pl.jkozal.weather_forecast.backend;

import javax.inject.Singleton;

import dagger.Component;
import pl.jkozal.weather_forecast.viewcontroller.RestFragment;

/**
 * Created by jacek on 31.03.2018.
 */

@Singleton
@Component(modules={RestModule.class})
public interface RestComponent {
    void inject(RestFragment fragment);
}
